#include <iostream>
#include <gtk/gtk.h>
#include <pthread.h>
#include <vector>
#include <time.h>
#include <gdk/gdkkeysyms.h>
#include "HeroPlane.h"
#include "EnemyPlane.h"
#include "Bullet.h"
#include "time.h"
#include "Boom.h"
#include <sstream>
#include <sys/select.h>
#include <string>
#include <stdlib.h>


#define WD_WIDTH 400
#define WD_HEIGHT 645
#define REFRESH_WD 30  //FPS帧率
#define PLANE_STEP 16 //飞机速度
#define BULLET_STEP 20 //子弹速度
#define HERO_WIDTH  97
#define HERO_HEIGHT  124
#define ENEMYPLANE_STEP 3 //敌机移动速度
#define ENEMYPLANE_NUM 5 //每次敌机产生数量
#define ENEMYPLANE_FRESH_TIME 3 //敌机n秒产生一次
using namespace std;
GtkWidget *window;
GtkWidget *layout;        //使用的布局
//GtkWidget *FPSlog;      //fps帧率实时显示
GtkWidget *heroimg;     //英雄图片
HeroPlane hero;         //主角飞机
gboolean keypresslist[] = {false,false,false,false,false}; //上 右 下 左 空格
int realfps = 0;
vector<EnemyPlane *> enemylist; //怪物列表
vector<Bullet *> bullets;
vector<Boom *> boomlist;
void UIthread();//UI线程刷新
void *FPStotal(void*);//实际帧率统计
bool gameover = false;
void freshScreen();


gboolean deal_keybord_event(GtkWidget *widget,GdkEventKey *event,gpointer data);//键盘消息机制
gboolean release_keybord_event(GtkWidget *widget,GdkEventKey *event,gpointer data);
static gboolean time_handler();
void init();
void CreateEnemy(int MaxplaneNum,vector<EnemyPlane *> &planelist);


static void sleep_ms(unsigned int secs)

{

    struct timeval tval;

    tval.tv_sec=secs/1000;

    tval.tv_usec=(secs*1000)%1000000;

    select(0,NULL,NULL,NULL,&tval);

}
int main(int argc,char *argv[])
{

    /*创建线程*/

    if (!g_thread_supported())
    {
        g_thread_init(NULL);
        gdk_threads_init();
    }

    /*各项初始化*/
    gtk_init(&argc,&argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window),"飞机大战");
    gtk_widget_set_size_request(window,WD_WIDTH,WD_HEIGHT);
    gtk_window_set_resizable(GTK_WINDOW(window),false);
    g_signal_connect(window,"key-press-event",G_CALLBACK(deal_keybord_event),NULL);
    g_signal_connect(window,"key-release-event",G_CALLBACK(release_keybord_event),NULL);
    //g_timeout_add(3000,(GSourceFunc)time_handler,NULL);


    /*背景贴图*/

    init();

    gtk_widget_show_all(window);


    //创建gtk的刷新线程  UI线程
    g_thread_create((GThreadFunc)UIthread, NULL, FALSE, NULL);
    pthread_t totalthread;
    if(pthread_create(&totalthread,NULL,FPStotal,NULL) != 0 ){
        cout << "Create thread fail" << endl;
    }

    gdk_threads_enter();
    gtk_main();
    gdk_threads_leave();
}


 //子弹状态以及子弹位移
    vector<int> deletelist;
    //敌机删除队列
    vector<int> deleteEnemyList;
    int delcount = 0;
//UI线程刷新
void UIthread(){
    int sleeptime = 1000/REFRESH_WD;//间隔刷新时间
    int timecount = 0;
    while(true){
        sleep_ms(sleeptime);
        if(gameover)
            break;
        g_main_context_invoke(NULL,(GSourceFunc)freshScreen,NULL);

        realfps++;

    }
    //Gameover
        GtkWidget *over = gtk_image_new_from_file("./img/gameover.png");
        gdk_threads_enter();
        gtk_fixed_put(GTK_FIXED(layout),over,0,0);
        gdk_threads_leave();

        gdk_threads_enter();
        gtk_widget_show_all(layout);
        gdk_threads_leave();
}

void freshScreen(){
    //TODO:图片位移等数据的计算
        if(keypresslist[0] && hero.Getpos_y() >= 0)
            hero.Setpos_y(hero.Getpos_y() - PLANE_STEP);
        if(keypresslist[1] && hero.Getpos_x() <= WD_WIDTH - 107)
            hero.Setpos_x(hero.Getpos_x() + PLANE_STEP);
        if(keypresslist[2] && hero.Getpos_y() <= WD_HEIGHT - 134)
            hero.Setpos_y(hero.Getpos_y() + PLANE_STEP);
        if(keypresslist[3] && hero.Getpos_x() >= 0)
            hero.Setpos_x(hero.Getpos_x() - PLANE_STEP);


        gtk_fixed_move(GTK_FIXED(layout),heroimg,hero.Getpos_x(),hero.Getpos_y());
        //gtk_fixed_move(GTK_FIXED(layout),enemylist[0]->GetImage(),enemylist[0]->Getpos_x(),enemylist[0]->Getpos_y());
        //敌机行为
        for(int i = 0;i < enemylist.size();i++){
            enemylist[i]->Setpos_y(enemylist[i]->Getpos_y() + ENEMYPLANE_STEP);
            if(enemylist[i]->Getpos_y() > WD_HEIGHT - 57){
                deleteEnemyList.push_back(i);
                continue;
            }
            if(enemylist[i]->GetStatus() == 1){
                gtk_fixed_move(GTK_FIXED(layout),enemylist[i]->GetImage(),enemylist[i]->Getpos_x(),enemylist[i]->Getpos_y());
            }else if(enemylist[i]->GetStatus() == 0){
                enemylist[i]->SetStatus(1);
                gtk_fixed_put(GTK_FIXED(layout),enemylist[i]->GetImage(),enemylist[i]->Getpos_x(),enemylist[i]->Getpos_y());
            }

            if(((enemylist[i]->Getpos_x() >= hero.Getpos_x() && enemylist[i]->Getpos_x() < hero.Getpos_x() + HERO_WIDTH) ||
                (enemylist[i]->Getpos_x()+50 > hero.Getpos_x() && enemylist[i]->Getpos_x() + 50 <= hero.Getpos_x() + HERO_WIDTH))
            && (enemylist[i]->Getpos_y() + 57 > hero.Getpos_y() && enemylist[i]->Getpos_y() + 57 < hero.Getpos_y() + HERO_HEIGHT)){

                gameover = true;
            }
        }


        //子弹的每一帧处理
        for(int i = 0;i < bullets.size();i++){
            //子弹每一帧的位移
            bullets[i]->SetPosy(bullets[i]->GetPosy() - BULLET_STEP);
            //若子弹超出边界，存入删除队列中。
            if(bullets[i]->GetPosy() < 0){
                deletelist.push_back(i);
                continue;
            }
            if(bullets[i]->GetStatus() == 1){
                gtk_fixed_move(GTK_FIXED(layout),bullets[i]->GetImage(),bullets[i]->GetPosx(),bullets[i]->GetPosy());
            }else if (bullets[i]->GetStatus() == 0){
                bullets[i]->SetStatu(1);
                gtk_fixed_put(GTK_FIXED(layout),bullets[i]->GetImage(),bullets[i]->GetPosx(),bullets[i]->GetPosy());
            }

            //判定飞机碰撞
            for(int j = 0;j < enemylist.size();j++){
                if(enemylist[j]->Getpos_x() + 50 >= bullets[i]->GetPosx() && bullets[i]->GetPosx()>=enemylist[j]->Getpos_x() && bullets[i]->GetPosy()  < enemylist[j]->Getpos_y() + 57){
                    deleteEnemyList.push_back(j);
                    deletelist.push_back(i);
                }
            }
        }


        //////////////////////////////////////释放空间部分////////////////////////////////////////////
        //释放子弹。
        for(int i = 0;i < deletelist.size();i++){
                //gtk_fixed_move(GTK_FIXED(layout),bullets[deletelist[i]]->GetImage(),bullets[deletelist[i]]->GetPosx(),bullets[deletelist[i]]->GetPosy());

                gtk_widget_destroy(GTK_WIDGET(bullets[deletelist[i] - delcount]->GetImage()));
                delete(bullets[deletelist[i] - delcount]);             //释放指针对应堆空间
                bullets.erase(bullets.begin() + deletelist[i] - delcount);//删除向量中的指针。
                delcount++;
        }
        deletelist.clear();
        delcount = 0;
        //释放被撞敌机
        for(int i = 0;i < deleteEnemyList.size();i++){
                char sr[50] = {0};
                Boom * boom = new Boom();
                boom->Setpos_x(enemylist[deleteEnemyList[i] - delcount]->Getpos_x());
                boom->Setpos_y(enemylist[deleteEnemyList[i] - delcount]->Getpos_y());

                sprintf(sr,"./img/boom/boom_%d.png",boom->Getppic_pos());
                boom->Setimage(gtk_image_new_from_file(sr));
                gtk_fixed_put(GTK_FIXED(layout),boom->Getimage(),boom->Getpos_x(),boom->Getpos_y());
                boomlist.push_back(boom);


                gtk_widget_destroy(GTK_WIDGET(enemylist[deleteEnemyList[i] - delcount]->GetImage()));
                delete(enemylist[deleteEnemyList[i] - delcount]);             //释放指针对应堆空间
                enemylist.erase(enemylist.begin() + deleteEnemyList[i] - delcount);//删除向量中的指针。
                delcount++;
        }
        deleteEnemyList.clear();
        delcount = 0;

        for(int i = 0;i < boomlist.size();i++){
            if(boomlist[i]->Getppic_pos() > 30){
                gtk_widget_destroy(boomlist[i]->Getimage());
                delete(boomlist[i]);
                boomlist.erase(boomlist.begin() + i);
                i--;
            }else{
                char sr[50] = {0};
                boomlist[i]->SetPic_pos(boomlist[i]->Getppic_pos() + 1);
                sprintf(sr,"./img/boom/boom_%d.png",boomlist[i]->Getppic_pos());
                gtk_image_set_from_file(GTK_IMAGE(boomlist[i]->Getimage()),sr);
            }
        }

        //更新界面
        gtk_widget_show_all(layout);
}

//键盘按下事件
gboolean deal_keybord_event(GtkWidget *widget,GdkEventKey *event,gpointer data){

    switch(event->keyval){
        case GDK_KEY_Up:
            keypresslist[0] = true;
			break;
        case GDK_KEY_Down:
            keypresslist[2] = true;
            break;
		case GDK_KEY_Left:
            keypresslist[3] = true;
			break;
		case GDK_KEY_Right:
            keypresslist[1] = true;
			break;
    }
    return TRUE;
}
//键盘释放事件
gboolean release_keybord_event(GtkWidget *widget,GdkEventKey *event,gpointer data){
    switch(event->keyval){
        case GDK_KEY_Up:
            keypresslist[0] = false;
			break;
        case GDK_KEY_Down:
            keypresslist[2] = false;
            break;
		case GDK_KEY_Left:
            keypresslist[3] = false;
			break;
		case GDK_KEY_Right:
            keypresslist[1] = false;
			break;
        case GDK_KEY_space:
            GtkWidget * img = gtk_image_new_from_file("./img/bullet.png");
            Bullet *but = new Bullet(hero.Getpos_x() + 42,hero.Getpos_y());
            but->SetImage(img);
            bullets.push_back(but);

			break;
    }
    return TRUE;
}

//初始化
void init(){
    layout = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER (window), layout);
    gtk_widget_show(layout);

    GtkWidget *backimg = gtk_image_new_from_file("./img/background.png");
    gtk_fixed_put(GTK_FIXED(layout), backimg, 0, 0);
    heroimg = gtk_image_new_from_file("./img/hero0.png");
    gtk_fixed_put(GTK_FIXED(layout), heroimg, 0, 0);
    //FPSlog = gtk

}

//屏幕中最大敌机数量。 随机创建敌机
void CreateEnemy(int MaxplaneNum,vector<EnemyPlane *> &planelist){
    srand((unsigned)time(NULL));
    int prepix = WD_WIDTH / MaxplaneNum;
    int planenum = rand()%MaxplaneNum;
    int pcount = 0;
    int pixs = 0;
    vector<bool> enemyplanes(MaxplaneNum,false);
    while(pcount != planenum){
        int px = rand()%MaxplaneNum;
        enemyplanes[px] = true;
        pcount++;
    }
    for(int i = 0;i < enemyplanes.size();i++){
        if(enemyplanes[i]){
            pixs = i * prepix;
            EnemyPlane * e1 = new EnemyPlane();
            e1->SetImage(gtk_image_new_from_file("./img/airplane.png"));
            e1->Setpos_x(pixs);
            planelist.push_back(e1);
        }
    }


}

void *FPStotal(void*){
    int timecount = 0;
    pthread_mutex_t pid;
    while(true){
        sleep(1);
        timecount += 1;
        if(timecount >= ENEMYPLANE_FRESH_TIME){
            timecount = 0;
            //pthread_mutex_lock(&pid);
            CreateEnemy(ENEMYPLANE_NUM,enemylist);
            //pthread_mutex_unlock(&pid);

        }
        system("clear");
        cout << "FPS:" << realfps << endl;
        realfps = 0;
    }
}

static gboolean time_handler(){
    CreateEnemy(ENEMYPLANE_NUM,enemylist);
    return TRUE;
}
