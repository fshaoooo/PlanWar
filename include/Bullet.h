#ifndef BULLET_H
#define BULLET_H
#include <gtk/gtk.h>


class Bullet
{
    public:
        Bullet(int x,int y);
        virtual ~Bullet();

        GtkWidget *image;
        int GetPosx(){return pos_x;};
        void SetPosx(int x){pos_x = x;};
        int GetPosy(){return pos_y;};
        void SetPosy(int y){pos_y= y;};
        GtkWidget *GetImage(){return image;}
        void SetImage(GtkWidget *img){image = img;}

        int GetStatus(){return status;};
        void SetStatu(int st){status = st;};

    protected:

    private:
        int pos_x;
        int pos_y;
        int status;
};

#endif // BULLET_H
