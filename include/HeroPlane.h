#ifndef HEROPLANE_H
#define HEROPLANE_H
#include <gtk/gtk.h>


class HeroPlane
{
    public:
        HeroPlane();
        virtual ~HeroPlane();

        int Getpos_x() { return pos_x; }
        void Setpos_x(int val) { pos_x = val; }
        int Getpos_y() { return pos_y; }
        void Setpos_y(int val) { pos_y = val; }

    protected:

    private:
        int pos_x;
        int pos_y;
};

#endif // HEROPLANE_H
