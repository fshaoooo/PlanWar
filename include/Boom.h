#ifndef BOOM_H
#define BOOM_H
#include <gtk/gtk.h>


class Boom
{
    public:
        Boom();
        virtual ~Boom();

        int Getpos_x() { return pos_x; }
        void Setpos_x(int val) { pos_x = val; }
        int Getpos_y() { return pos_y; }
        void Setpos_y(int val) { pos_y = val; }
        GtkWidget * Getimage() { return image; }
        void Setimage(GtkWidget * val) { image = val; }
        int Getppic_pos(){return pic_pos;}
        void SetPic_pos(int x){pic_pos = x;}
    protected:

    private:
        int pos_x;
        int pos_y;
        GtkWidget * image;
        int pic_pos;
};

#endif // BOOM_H
