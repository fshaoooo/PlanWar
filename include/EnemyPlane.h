#ifndef ENEMYPLANE_H
#define ENEMYPLANE_H
#include <gtk/gtk.h>


class EnemyPlane
{
    public:
        EnemyPlane();
        virtual ~EnemyPlane();
        GtkWidget *image;

        int Getpos_x() { return pos_x; }
        void Setpos_x(int val) { pos_x = val; }
        int Getpos_y() { return pos_y; }
        void Setpos_y(int val) { pos_y = val; }
        GtkWidget *GetImage(){return image;}
        void SetImage(GtkWidget *img){image = img;}
        int GetStatus(){return status;}
        void SetStatus(int st){status = st;}

    protected:

    private:
        int pos_x;
        int pos_y;
        int status;
};

#endif // ENEMYPLANE_H
